#![feature(proc_macro_hygiene)]

use failure::{Fallible, format_err};
use lazy_static::lazy_static;
use regex::Regex;
use std::fs::File;
use std::io::{BufRead, BufReader};

lazy_static! {
    static ref RE: Regex = Regex::new(r"(?P<name>[[:alpha:]]+)\s+(?P<code>#[[:xdigit:]]{6})").unwrap();
}

struct Color {
    name: String,
    hex: String,
    rgb: [u8; 3]
}

fn proximo(first: &Color, second: &Color) -> u32 {
    let mut res = 0;
    for n in 0..3 {
        res += (first.rgb[n] as i32 - second.rgb[n] as i32).abs() as u32;
    }
    res
}

fn colors(file: &File) -> Vec<Color> {
    BufReader::new(file).lines()
                        .filter_map(Result::ok)
                        .filter_map(|line| {
        let captures = RE.captures(line.as_str())?;
        let name = captures.name("name")?.as_str().to_owned();
        let hex = captures.name("code")?.as_str().to_owned();

        let r: u8 = u8::from_str_radix(&hex[1..3], 16).ok()?;
        let g: u8 = u8::from_str_radix(&hex[3..5], 16).ok()?;
        let b: u8 = u8::from_str_radix(&hex[5..7], 16).ok()?;
        Some(Color { name, hex, rgb: [r, g, b] })
    }).collect()
}

fn main() -> Fallible<()> {
    let mut args = std::env::args();
    let arg = args.nth(1).ok_or(format_err!("First file arg missing!"))?;
    let file = File::open(arg).map_err(|_| format_err!("First arg must be a file!"))?;
    let first = colors(&file);

    let arg = args.last().ok_or(format_err!("Second file arg missing!"))?;
    let file = File::open(arg).map_err(|_| format_err!("Second arg must be a file!"))?;
    let second = colors(&file);

    let mut rows = Vec::new();

    for color in &first {
        let mut cur = std::u32::MAX;
        let mut best = second.first().expect("No colors in second file!");
        for comp in &second {
            let prox = proximo(&color, &comp);
            if prox < cur {
                cur = prox;
                best = comp;
            }
        }

        rows.push(maud::html! {
            tr {
                td (&color.name)
                td (&color.hex)
                td style={ "background: " (color.hex) } {}
                td (&best.name)
                td (&best.hex)
                td style={ "background: " (best.hex) } {}
            }
        });
    }

    let html = maud::html! {
        (maud::DOCTYPE)
        html {
            head {
                style "
                td {
                    padding: 0 1rem;
                }
                "
            }
            body {
                table {
                    thead {
                        th "First"
                        th "Hex"
                        th "Color"
                        th "Second"
                        th "Hex"
                        th "Color"
                    }
                    tbody {
                        @for row in rows {
                            (row)
                        }
                    }
                }
            }
        }
    };

    println!("{}", html.into_string());

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn max_diff_colors() {
        let first = Color { name: String::new(), hex: String::new(), rgb: [255, 255, 255]};
        let second = Color { name: String::new(), hex: String::new(), rgb: [0, 0, 0]};
        assert_eq!(proximo(&first, &second), 195075)
    }

    #[test]
    fn max_inv_colors() {
        let first = Color { name: String::new(), hex: String::new(), rgb: [0, 0, 0]};
        let second = Color { name: String::new(), hex: String::new(), rgb: [255, 255, 255]};
        assert_eq!(proximo(&first, &second), 195075)
    }

    #[test]
    fn same_colors() {
        let first = Color { name: String::new(), hex: String::new(), rgb: [255, 255, 255]};
        let second = Color { name: String::new(), hex: String::new(), rgb: [255, 255, 255]};
        assert_eq!(proximo(&first, &second), 0);
    }
}